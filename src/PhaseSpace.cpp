/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

#include "PhaseSpace.h"

void TestPhaseSpace() {
    //		TPlanarPhaseSpace<float> ps;
    using TPS = TZAxisStereographicSphericalPhaseSpace<double>;
    TPS ps2;
    TVec3d center{0, 0, 2};
    double radius = 2;
    ps2 = TPS(center, radius);
    TVec3d loc3{1, 1.1, -0.5};
    TVec3d dir3{0, 0, -1};
    bool b;
    TVec3d loc3Sphere;
    std::tie(b, loc3Sphere) =
        SphereIntersect<double>(center, radius, loc3, dir3);

    TPS::TLoc loc2 = ps2.Loc(loc3, dir3);
    auto loc3test = ps2.Loc_to_V3(loc2);
    auto dir2 = ps2.Dir(loc2, dir3);
    TVec3d dir3test = ps2.Dir_to_V3(loc2, dir2);

    // use debugger to see if loc3test==loc3 and dir3test=dir3
}
