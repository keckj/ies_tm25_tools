/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

// TM25Util.h: Utility functions

#ifndef TM25UTIL_H_YR7IO8Z4
#define TM25UTIL_H_YR7IO8Z4

#include <array>
#include <locale>
#include <string>
#include <vector>

namespace TM25 {
// assign string from fixed length char array, taking 0 termination into account
template <std::size_t N>
void AssignCharArrayToString(const std::array<char, N> &source,
                             std::string &target);

template <std::size_t N>
void AssignChar32ArrayToString(const std::array<char32_t, N> &source,
                               std::u32string &target);

void AssignChar32ArrayToString(const std::vector<char32_t> &source,
                               std::u32string &target);

std::string ToString(const std::u32string &s, char notranslation = '?');

std::u32string ToU32String(const std::string &s);

// template definitions

template <std::size_t N>
void AssignCharArrayToString(const std::array<char, N> &source,
                             std::string &target) {
    target.clear();
    for (std::size_t i = 0; i < N; ++i) {
        char c = source[i];
        if (c != 0)
            target.push_back(c);
        else
            break;
    }
}

template <std::size_t N = 0>
void AssignChar32ArrayToString(const std::array<char32_t, N> &source,
                               std::u32string &target) {
    target.clear();
    for (std::size_t i = 0; i < N; ++i) {
        char32_t c = source[i];
        if (c != 0)
            target.push_back(c);
        else
            break;
    }
}
} // namespace TM25

#endif /* end of include guard: TM25UTIL_H_YR7IO8Z4 */
