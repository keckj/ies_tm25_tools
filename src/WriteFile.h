/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

#ifndef WRITEFILE_H_UF6CTZMJ
#define WRITEFILE_H_UF6CTZMJ

#include <array>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>

namespace TM25 {
class TWriteFile {
  public:
    explicit TWriteFile(const std::string &fn);
    // opens file for binary writing

    template <typename T> size_t Write(const T &t);
    // writes the binary representation of t to file
    // returns # of bytes written == sizeof(T)

    size_t WriteCString(const std::string &s);
    size_t WriteCString(const char *s);
    // wrítes null terminated character string, returns bytes written = length +
    // 1

    size_t WriteUTF32TextBlock(const std::u32string &s, size_t nBytes);
    // writes at most nBytes % 4 characters from s, then writes 0 until bBytes
    // written

    template <typename Iterator> // for u32string fields
    size_t WriteRange(Iterator begin, Iterator end);

    size_t WriteZeroBytes(size_t n);

    template <typename T, size_t N>
    size_t WriteArray(const std::array<T, N> &t);

    template <typename T> size_t WriteVector(const std::vector<T> &t);

    void WriteBytes(const void *p, size_t n);

    size_t BytesWritten() const;

    ~TWriteFile();

  private:
    std::vector<char> buf_;
    static const size_t buf_size_ = 10 * 1024 * 1024; // write chunks of 10 MB;
    size_t bufpos_;
    size_t written_;
    std::ofstream f_;

    void WriteBuf();
    void WriteBytesDirect(const void *p, size_t n);
};

template <typename T> inline size_t TWriteFile::Write(const T &t) {
    WriteBytes(&t, sizeof(T));
    return sizeof(T);
}

template <typename Iterator>
inline size_t TWriteFile::WriteRange(Iterator begin, Iterator end) {
    size_t rv = 0;
    using it = Iterator;
    for (it i = begin; i != end; ++i)
        rv += Write(*i);
    return rv;
}

template <typename T, size_t N>
inline size_t TWriteFile::WriteArray(const std::array<T, N> &t) {
    size_t n = N * sizeof(T);
    WriteBytes(t.data(), n);
    return n;
}

template <typename T>
inline size_t TWriteFile::WriteVector(const std::vector<T> &t) {
    size_t n = t.size() * sizeof(T);
    WriteBytes(t.data(), n);
    return n;
}

} // namespace TM25

#endif /* end of include guard: WRITEFILE_H_UF6CTZMJ */
