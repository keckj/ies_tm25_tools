/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

// implementation of TM25Util.h
#include <vector>

#include "TM25Util.h"

namespace TM25 {

std::string ToString(const std::u32string &s, char notranslation) {
    std::vector<char> rv(s.length());
    std::locale loc = std::locale();
    std::use_facet<std::ctype<std::u32string::value_type>>(loc).narrow(
        s.data(), s.data() + s.length(), notranslation, rv.data());
    return std::string(rv.data(), rv.size());
}

std::u32string ToU32String(const std::string &s) {
    std::vector<char32_t> rv(s.length());
    std::locale loc = std::locale();
    std::use_facet<std::ctype<std::u32string::value_type>>(loc).widen(
        s.data(), s.data() + s.length(), rv.data());
    return std::u32string(rv.data(), rv.size());
}

void AssignChar32ArrayToString(const std::vector<char32_t> &source,
                               std::u32string &target) {
    target.clear();
    size_t N = source.size();
    for (std::size_t i = 0; i < N; ++i) {
        char32_t c = source[i];
        if (c != 0)
            target.push_back(c);
        else
            break;
    }
}
} // namespace TM25
