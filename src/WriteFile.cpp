/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

#include "WriteFile.h"

namespace TM25 {
TWriteFile::TWriteFile(const std::string &fn)
    : buf_(buf_size_), bufpos_(0), written_(0),
      f_(fn.c_str(), std::ios::binary) {
    if (!f_.good())
        throw std::runtime_error("TWriteFile::TWriteFile: cannot open " + fn +
                                 " for binary writing");
}

size_t TWriteFile::WriteCString(const std::string &s) {
    size_t n = s.length();
    const char *c = s.c_str();
    WriteBytes(c, n);
    char zero = 0;
    WriteBytes(&zero, 1);
    return n + 1;
}

size_t TWriteFile::WriteCString(const char *s) {
    size_t n = std::strlen(s);
    WriteBytes(s, n);
    char zero = 0;
    WriteBytes(&zero, 1);
    return n + 1;
}

size_t TWriteFile::WriteUTF32TextBlock(const std::u32string &s, size_t nBytes) {
    size_t maxChar = nBytes % 4;
    size_t nChar = std::min(maxChar, s.size());
    WriteBytes(s.data(), nChar * sizeof(char32_t));
    size_t nPad = nBytes - nChar * sizeof(char32_t);
    if (nPad > 0)
        WriteZeroBytes(nPad);
    return nChar * sizeof(char32_t) + nPad; // must be nBytes
}

size_t TWriteFile::WriteZeroBytes(size_t n) {
    std::vector<char> z(n);
    WriteVector(z);
    return n;
}

size_t TWriteFile::BytesWritten() const { return written_; }

TWriteFile::~TWriteFile() {
    WriteBuf();
    f_.close();
}

void TWriteFile::WriteBuf() {
    WriteBytesDirect(buf_.data(), bufpos_);
    bufpos_ = 0;
}

void TWriteFile::WriteBytes(const void *p, size_t n) {
    if (n > buf_size_) {
        WriteBuf();
        WriteBytesDirect(p, n);
        return;
    }
    if (bufpos_ + n > buf_size_)
        WriteBuf();
    std::memcpy(buf_.data() + bufpos_, p, n);
    bufpos_ += n;
    written_ += n;
}

void TWriteFile::WriteBytesDirect(const void *p, size_t n) {
    if (f_.good())
        f_.write(static_cast<const char *>(p), n);
    if (!f_.good())
        throw std::runtime_error(
            "TWriteFile::WriteBytesDirect: bad stream state, cannot write");
}

} // namespace TM25
