[![Platform](https://img.shields.io/badge/platform-Linux-lightgrey.svg)]()
[![C++17](https://img.shields.io/badge/C++-17-blue.svg?style=flat&logo=c%2B%2B)]()
[![License: Unlicense](https://img.shields.io/badge/license-Unlicense-green.svg)](http://unlicense.org/)
[![Pipeline Status](https://gitlab.com/keckj/ies_tm25_tools/badges/main/pipeline.svg)](https://gitlab.com/keckj/ies_tm25_tools/-/commits/main)
[![Docker Pulls](https://img.shields.io/docker/pulls/keckj/ies_tm25_tools.svg)](https://hub.docker.com/r/keckj/ies_tm25_tools/tags)

<img src="assets/logo.png"  width="700">

A fork/port of [TM25RaySetTools](https://github.com/JuliusMuschaweck/TM25RaySetTools) to Linux.

This repository provides TM25RaySetTools as a library (libtm25) as well as two binaries:
- `asap_to_tm25`: convert an ASAP DIS rayfile to an IES TM25 rayfile.
- `interpolate_rayset`: interpolate rays from an existing IES TM25 rayfile.

Prebuilt binaries are available in the releases section.

# How to get started ?
You will need a C++17 compiler, cmake 3.9+ and a cmake generator such as GNU make.
To install required dependencies on Ubuntu, look at the [Dockerfile](ci/docker/ubuntu/focal_fossa/Dockerfile).
```
git clone https://github.com/keckj/ies_tm25_tools.git
cd ies_tm25_tools/build
cmake ..
cmake --build .
./interpolate_rayset ../assets/rayfile_LERTDUW_S2WP_green_100k_20161013_IES_TM25.TM25RAY
./asap_to_tm25 ../assets/rayfile_LUW_HWQP_blue_100k_20160518_ASAP.DIS
```

# How to install the library and tools ?
```
git clone https://github.com/keckj/ies_tm25_tools.git
cd ies_tm25_tools
cmake -B build
cmake --build ./build --parallel
cmake --install ./build --prefix /usr/local
```

# TM25RaySetTools
A set of C++ tools for IES-TM25 ray sets
Created by Julius Muschaweck, JMO GmbH, Gauting, Germany (julius@jmoptics.de)

Background: In illumination optics, ray files are often used as source models
in Monte Carlo simulations.
Ray files are typically large, containing millions of random rays. Traditionally,
each optical simulation software vendor uses his proprietary file format, which
is not only a web space and quality control nightmare for the LED vendors. The
proprietary formats are also often poorly documented and not flexible.
Therefore, an IES committee created a standardized, well engineered, flexible,
compact and well documented file format, containing a superset of all features of all
major proprietary formats. This file format is called IES-TM25-13, or TM25 for short.
The standard, published in 2013, is available at
https://www.ies.org/product/ray-file-format-for-the-description-of-the-emission-property-of-light-sources/

TM25 ray files are now becoming more common. Simulation software vendors have
implemented the ability to read TM25 ray files, and LED vendors provide them.
For my own scientific and engineering work, I need direct access to TM25 ray file
content. Therefore, I wrote C++ software to parse TM25 ray files and provide the
content in a C++ class, and I make this software available to the community under
the "unlicense", see unlicense.txt, which basically allows you to do whatever you
want with the code, except sueing me if it doesn't work for you.

Currently, the software is available as a Visual Studio 2017 project. I have tried
my best to write it in portable way, using only C++ constructs from the C++11 standard.
I'm planning to test it on a Linux/gcc platform as well, and to add more features.

If you're interested in the InterpolateRaySet project I'll present at DGaO in June,
build it with Visual Studio 2017, and set the command line parameters to the
file name of the configuration file, InterpolateRaySetTest.cfg.

As always, I'm looking forward to your feedback. Please use the standard GitHub workflow,
adding issues, and pull requests if you'd like to provide your own additions / bug fixes.

Enjoy!

Gauting, Germany, on a sunny day in 2019
Julius Muschaweck
