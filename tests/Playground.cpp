/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

#include <array>
#include <fstream>
#include <iostream>
#include <vector>

#include "KDTree.h"

void TestBuf() {
    std::ifstream f(
        "../assets/rayfile_LERTDUW_S2WP_blue_100k_20161013_IES_TM25.TM25RAY",
        std::ios::binary);
    std::vector<char> buf(1024 * 1024 * 10);
    size_t n = buf.size();
    size_t n_read = f.rdbuf()->sgetn(buf.data(), n);
    bool b = f.eof(); // false! buffer knows nothing about file which owns it
}

int main() {
    std::cout << "Hello World!" << std::endl;
    if (KDTree::Def::dim == 2)
        KDTree::TestKDTree2D("../assets/TestKDTree.m");
    if (KDTree::Def::dim == 4)
        KDTree::TestKDTree4D();
    TestBuf();
    std::cout << std::endl;
}
