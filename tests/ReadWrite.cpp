/*
 * Provided by Julius Muschaweck, JMO GmbH, Gauting to the public domain
 * under the Unlicense, see LICENSE in the repository or http://unlicense.org/
 * 2019-01-19
 *
 * ======================================================================
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 * ======================================================================
 */

// ReadWrite.cpp : Defines the entry point for the console application.
// and provides a typical usage example.

#include <iostream>

#include "TM25.h"

int main() {
    try {
        TM25::TTM25RaySet rs;
        // this small ray file is on GitHub
        rs.Read("../assets/"
                "rayfile_LERTDUW_S2WP_green_100k_20161013_IES_TM25.TM25RAY");
        // this 500 MB ray file with 20 million rays can be downloaded from
        // www.osram-os.com
        // rs.Read("../rayfile_LERTDUW_S2WP_green_20M_20161013_IES_TM25.TM25RAY");
        for (auto w : rs.Warnings()) {
            std::cout << w << "\n";
        }
        auto bb = rs.RayArray().BoundingBox();
        std::cout << "Bounding Box: x in [" << bb.first[0] << ','
                  << bb.second[0] << "], y in [" << bb.first[1] << ','
                  << bb.second[1] << "], z in [" << bb.first[2] << ','
                  << bb.second[2] << "]" << std::endl;
    } catch (TM25::TM25Error &e) {
        std::cout << e.what() << std::endl;
    } catch (std::runtime_error &e) {
        std::cout << "std::runtime_error: " << e.what() << std::endl;
    } catch (...) {
        std::cout << "unknown error" << std::endl;
    }
    return 0;
}
